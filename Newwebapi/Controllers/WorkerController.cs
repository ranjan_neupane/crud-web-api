﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newwebapi.Models;

namespace Newwebapi.Controllers
{
    public class WorkerController : ApiController
    {
        //Get data-retrive data
        public IHttpActionResult GetAllWorker()
        {
            IList<WorkerViewModel> workers = null;
            using(var x=new NewwebEntities())
            {
                    workers=x.Workers.Select(c => new WorkerViewModel()
                    {
                      Id =c.Id,
                      Name=c.Name,
                      Work=c.Work
                    }).ToList<WorkerViewModel>();
            }


            if (workers.Count == 0)
                return NotFound();

            return Ok(workers);
                    
        }

        //Post -Insert new Data
        public IHttpActionResult PostNewWorker(WorkerViewModel worker)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data");

            using (var x = new NewwebEntities())
            {
                x.Workers.Add(new Worker()
                { 
                    Id = worker.Id,
                    Name = worker.Name,
                    Work = worker.Work,


                });


                x.SaveChanges();
            }

            return Ok();
           }

        //put data-update dat ausing id
        public IHttpActionResult PutWorker(WorkerViewModel worker)
        {
            if (!ModelState.IsValid)
                return BadRequest("This is inavalid");


            using(var x= new NewwebEntities())
            {
                var checkExsitingWorker = x.Workers.Where(c => c.Id == worker.Id).FirstOrDefault<Worker>();

                if (checkExsitingWorker != null)
                {
                    checkExsitingWorker.Id = worker.Id;
                    checkExsitingWorker.Name = worker.Name;
                    checkExsitingWorker.Work = worker.Work;

                    x.SaveChanges();

                }
                else
                    return NotFound();

            }
            return Ok();


        }

        //Delete data 
        public IHttpActionResult Delete(int Id)
        {
            if (Id <= 0)
                return BadRequest("Please enter valid Customer id");

            using(var x=new NewwebEntities())
            {
                var worker = x.Workers
                    .Where(c => c.Id == Id)
                    .FirstOrDefault();

                x.Entry(worker).State = System.Data.Entity.EntityState.Deleted;
                x.SaveChanges();
            }
            return Ok();
        }
    }
}
